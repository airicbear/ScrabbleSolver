# ScrabbleSolver

## Description
Calculate all possible words and highest scoring word given a set of letters.

## Controls
### Mouse
Left click - select/place/pick up tile  
Right click/`Esc` - return tile
### Keyboard
`q` - previous player  
`e` - next player  
`c` - change color theme  
`v` - default color theme  
`1` - show best word and possible words from current tiles  
letter - assign value to a selected blank tile
### On-screen
Reset button - resets the game  
Draw button - draw tiles for the current player
