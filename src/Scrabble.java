import java.util.Random;

import processing.core.PApplet;

/**
 * https://github.com/Popguy7843/ScrabbleSolver
 * <h1>ScrabbleSolver</h1>
 * <h2>Controls</h2>
 * <ul>
 * <h2>Mouse</h2>
 * <li>Left click - select/place/pick up tile</li>
 * <li>Right click/Esc - return tile</li>
 * <h2>Keyboard</h2>
 * <li>q - previous turn</li>
 * <li>e - next turn</li>
 * <li>c - change color theme</li>
 * <li>v - default color theme</li>
 * <li>1 - show best word and possible words</li>
 * <li>letter - assign value to a selected blank tile</li>
 * <h2>On-screen</h2>
 * <li>Reset button - resets the game</li>
 * <li>Draw button - draw tiles for the current player</li>
 * </ul>
 */
public class Scrabble extends PApplet {

  private final byte NUMPLAYERS = 4;
  private ScrabbleGame scrabbleGame = new ScrabbleGame(NUMPLAYERS);
  private float RED = 172;
  private float GREEN = 171;
  private float BLUE = 141;
  // Default Color - RED = 172, GREEN = 171, BLUE = 141
  // Dark color - RED-57, GREEN-70, BLUE-73
  // Light color - RED+43, GREEN+30, BLUE+27
  
  public static void main(String[] args) {
    PApplet.main("Scrabble");
  }
  
  public void settings() {
    size(scrabbleGame.getWIDTH(), scrabbleGame.getHEIGHT());
  }
  
  public void setup() {
    background(RED, GREEN, BLUE);
    drawGame(scrabbleGame);
  }
  
  public void draw() {
    background(RED, GREEN, BLUE);
    drawGame(scrabbleGame);
    handOnHover(scrabbleGame);
  }
  
  public void mouseClicked() {
    switch(mouseButton) {
      case LEFT:
        if(hovering(scrabbleGame.getResetButton()) && scrabbleGame.getSelectedTile().isEmpty()) {
          scrabbleGame = new ScrabbleGame(NUMPLAYERS); // Reset
        } else if(hovering(scrabbleGame.getDrawButton()) && scrabbleGame.getSelectedTile().isEmpty()) {
          scrabbleGame.getCurrentPlayer().drawTiles(scrabbleGame.getTilesRemaining()); // Draw
        } else {
          selectTile(scrabbleGame);
          placeTile(scrabbleGame);
          if(scrabbleGame.noMoreTurns()) {
            scrabbleGame.setEndGame(true);
          }
        }
        break;
      case RIGHT: scrabbleGame.returnTile(); break;
      default: break;
    }
  }
  
  public void keyPressed() {
    if(scrabbleGame.getSelectedTile().isBlank()) {
      changeBlankTileValue(scrabbleGame, key);
    } else {
      switch(key) {
        case '1': toggleScrabbleSolver(scrabbleGame); break;
        case 'q': scrabbleGame.previousTurn(); break;
        case 'e': scrabbleGame.nextTurn(); break;
        case 'c': colorize(); break;
        case 'v': decolorize(); break;
        case ESC: key = 0; scrabbleGame.returnTile(); break;
        default: break;
      }
    }
  }
  
  private void colorize() {
    Random random = new Random();
    RED = random.nextFloat() * 255;
    GREEN = random.nextFloat() * 255;
    BLUE = random.nextFloat() * 255;
  }
  
  private void decolorize() {
    RED = 172;
    GREEN = 171;
    BLUE = 141;
  }
  
  private void showWinningPlayer(ScrabbleGame scrabbleGame) {
    drawTransparentBackground(scrabbleGame);
    pushStyle();
      textSize(42);
      textAlign(CENTER);
      text("WINNER\n" + scrabbleGame.winningPlayer().toString(), scrabbleGame.getWIDTH() / 2, scrabbleGame.getHEIGHT() / 2 - 100);
    popStyle();
  }
  
  private void drawGame(ScrabbleGame scrabbleGame) {
    drawBoard(scrabbleGame);
    drawCurrentPlayerName(scrabbleGame);
    drawCurrentPlayerScore(scrabbleGame);
    drawButton(scrabbleGame.getResetButton());
    drawButton(scrabbleGame.getDrawButton());
    drawCurrentTiles(scrabbleGame);
    if(scrabbleGame.isDisplayingScreenSolver()) {
      drawScrabbleSolver(scrabbleGame);
    } else if(scrabbleGame.isEndGame()) {
      showWinningPlayer(scrabbleGame);
    }
  }
  
  private void drawBoard(ScrabbleGame scrabbleGame) {
    for(Tile[] row : scrabbleGame.getBoard()) {
      for(Tile boardTile : row) {
        if(!boardTile.isEmpty()) {
          drawTile(boardTile);
        } else {
          drawEmptyTile(boardTile);
        }
      }
    }
  }
  
  private void drawCurrentTiles(ScrabbleGame scrabbleGame) {
    for(Tile t : scrabbleGame.getCurrentPlayer().getCurrentTiles()) {
      if(t.isSelected()) {
        setPositionToCursor(t);
      }
      if(!t.isEmpty()) {
        drawTile(t);
      } else {
        drawEmptyTile(t);
      }
    }
  }
  
  private void drawTile(Tile tile) {
    drawTileBackground(tile);
    drawTileLetter(tile);
    drawTileScore(tile);
  }
  
  private void drawTileBackground(Tile tile) {
    pushStyle();
      noStroke();
      switch(tile.getId()) {
        case 1: fill(RED+48, GREEN-38, BLUE-13); break; // Red
        case 2: fill(RED-41, GREEN+7, BLUE+67); break; // Blue
        case 3: fill(RED-23, GREEN+47, BLUE+41); break; // Green
        case 4: fill(RED+70, GREEN+59, BLUE+36); break; // Yellow
        default: fill(RED+43, GREEN+30, BLUE+27); break; // Light color
      }
      rect(tile.getX(), tile.getY(), tile.getSize(), tile.getSize());
    popStyle();
  }
  
  private void drawTileLetter(Tile tile) {
    pushStyle();
      fill(RED-57, GREEN-70, BLUE-73); // Dark color
      textSize(24);
      if(!tile.isEmpty() && !tile.isBlank()) {
        text(tile.toString(), tile.getX() + 12.5f, tile.getY() + tile.getSize() - 20);
      }
    popStyle();
  }
  
  private void drawTileScore(Tile tile) {
    pushStyle();
      fill(RED-57, GREEN-70, BLUE-73); // Dark color
      textSize(16);
      if(!tile.isEmpty() && !tile.isBlank()) {
        text(tile.score(), tile.getX() + 28.5f, tile.getY() + tile.getSize() - 5);
      }
    popStyle();
  }
  
  private void setPositionToCursor(Tile tile) {
    tile.setPosition(mouseX - tile.getSize() / 2, mouseY - tile.getSize() / 2);
  }
  
  private void drawScrabbleSolver(ScrabbleGame scrabbleGame) {
    drawTransparentBackground(scrabbleGame);
    drawBestWord(scrabbleGame);
    drawPossibleWords(scrabbleGame);
  }
  
  private void drawCurrentPlayerName(ScrabbleGame scrabbleGame) {
    pushStyle();
      fill(RED-57, GREEN-70, BLUE-73);
      textSize(24);
      text("Player " + scrabbleGame.getCurrentPlayer().getId(), 175, scrabbleGame.getHEIGHT() - 45);
    popStyle();
  }
  
  private void drawCurrentPlayerScore(ScrabbleGame scrabbleGame) {
    pushStyle();
      fill(RED-57, GREEN-70, BLUE-73);
      textSize(24);
      text("Score: " + scrabbleGame.getCurrentPlayer().getScore(), scrabbleGame.getWIDTH() - 270, scrabbleGame.getHEIGHT() - 45);
    popStyle();
  }  
  
  private void drawEmptyTile(Tile tile) {
    pushStyle();
      noStroke();
      fill(RED-57, GREEN-70, BLUE-73); // Dark color
      rect(tile.getInitX(), tile.getInitY(), tile.getSize(), tile.getSize());
    popStyle();
  }
  
  private void drawTransparentBackground(ScrabbleGame scrabbleGame) {
    pushStyle();
      noStroke();
      fill(0, 0, 0, 100);
      rect(0, 0, scrabbleGame.getWIDTH(), scrabbleGame.getHEIGHT());
    popStyle();
  }
  
  private void drawBestWord(ScrabbleGame scrabbleGame) {
    pushStyle();
      fill(RED+43, GREEN+30, BLUE+27);
      text("Best word: " + scrabbleGame.getBestWord(), 50, 50);
    popStyle();
  }
  
  private void drawPossibleWords(ScrabbleGame scrabbleGame) {
    pushStyle();
      fill(RED+43, GREEN+30, BLUE+27);
      text("Possible words: " + scrabbleGame.getPossibleWords(), 50, 100, scrabbleGame.getWIDTH() - 100, scrabbleGame.getHEIGHT());
    popStyle();
  }
  
  private void drawButton(Button button) {
    drawButtonBackground(button);
    drawButtonText(button);
  }
  
  private void drawButtonBackground(Button button) {
    pushStyle();
      noStroke();
      fill(RED+43, GREEN+30, BLUE+27); // Light color
      rect(button.getX(), button.getY(), button.getWidth(), button.getHeight());
    popStyle();
  }
  
  private void drawButtonText(Button button) {
    pushStyle();
      fill(RED-57, GREEN-70, BLUE-73); // Dark color
      textSize(42);
      text(button.getText(), button.getX() + 12.5f, button.getY() + button.getHeight() - 10);
    popStyle();
  }
  
  private void toggleScrabbleSolver(ScrabbleGame scrabbleGame) {
    if(scrabbleGame.isDisplayingScreenSolver()) {
      scrabbleGame.setDisplayingScreenSolver(false);
    } else {
      scrabbleGame.setDisplayingScreenSolver(true);
      scrabbleGame.updateScrabbleSolver();
    }
  }
  
  private void changeBlankTileValue(ScrabbleGame scrabbleGame, char key) {
    if(scrabbleGame.getSelectedTile().isBlank() && Character.isLetter(key)) {
      scrabbleGame.getSelectedTile().setValue(Character.toUpperCase(key));
      scrabbleGame.updateScrabbleSolver();
    }
  }
  
  private boolean hovering(Tile tile) {
    return (mouseX >= tile.getX() && mouseX <= tile.getX() + tile.getSize() &&
            mouseY >= tile.getY() && mouseY <= tile.getY() + tile.getSize());
  }
  
  private boolean hovering(Button button) {
    return (mouseX >= button.getX() && mouseX <= button.getX() + button.getWidth() &&
            mouseY >= button.getY() && mouseY <= button.getY() + button.getHeight());
  }
  
  private void handOnHover(ScrabbleGame scrabbleGame) {
    boolean hovering = 
        hoveringBoardTile(scrabbleGame) 
        || hoveringCurrentTile(scrabbleGame)
        || hovering(scrabbleGame.getDrawButton())
        || hovering(scrabbleGame.getResetButton());
    if(hovering) {
      cursor(HAND);
    } else {
      cursor(ARROW);
    }
  }
  
  private boolean hoveringBoardTile(ScrabbleGame scrabbleGame) {
    for(Tile[] row : scrabbleGame.getBoard()) {
      for(Tile tile : row) {
        if(hovering(tile)) {
          return true;
        }
      }
    }
    return false;
  }
  
  private boolean hoveringCurrentTile(ScrabbleGame scrabbleGame) {
    for(Tile tile : scrabbleGame.getCurrentPlayer().getCurrentTiles()) {
      if(hovering(tile)) {
        return true;
      }
    }
    return false;
  }
  
  private void selectTile(ScrabbleGame scrabbleGame) {
    for(Tile tile : scrabbleGame.getCurrentPlayer().getCurrentTiles()) {;
      if(hovering(tile)) {
        scrabbleGame.select(tile);
      }
    }
  }
  
  private void placeTile(ScrabbleGame scrabbleGame) {
    for(int r = 0; r < scrabbleGame.getBoard().length; r++) {
      for(int c = 0; c < scrabbleGame.getBoard()[r].length; c++) {
        if(hovering(scrabbleGame.getBoard()[r][c])) {
          if(!scrabbleGame.getBoard()[r][c].isEmpty()) {
            scrabbleGame.pickUpTile(r, c);
          } else {
            scrabbleGame.place(r, c);
          }
        }
      }
    }
  }
  
}