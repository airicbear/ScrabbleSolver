import java.util.ArrayList;
import java.util.Random;

public class Player {
  
  private final ArrayList<Tile> currentTiles;
  private final byte id;
  private int score;
  
  public Player(byte i) {
    id = i;
    currentTiles = new ArrayList<Tile>(7);
    score = 0;
  }
  
  public String toString() {
    return "Player " + id + ":\n" +
            " Score: " + score;
  }
  
  /**
   * Draw tiles for current player until current tiles are full
   */
  public void drawTiles(ArrayList<Tile> tiles) {
    for(Tile t : currentTiles) {
      drawTile(tiles, t);
    }
  }
  
  /**
   * <ol>
   * <li> Grabs a random tile from the tiles </li> 
   * <li> Moves random tile into the player's current tiles at index i </li>
   * </ol>
   */
  public void drawTile(ArrayList<Tile> tiles, Tile t) {
    if(tiles.size() > 0 && t.isEmpty()) {
      Random random = new Random();
      int randomInt = random.nextInt(tiles.size()-1);
      tiles.get(randomInt).setPosition(t.getInitX(), t.getInitY());
      tiles.get(randomInt).setInitPosition(t.getInitX(), t.getInitY());
      tiles.get(randomInt).setId(id);
      currentTiles.set(currentTiles.indexOf(t), tiles.get(randomInt));
      tiles.remove(randomInt);
    }
  }
  
  /**
   * <ol>
   * <li> Initialize currentTiles </li>
   * <li> Set currentTiles position </li>
   * </ol>
   */
  public void resetCurrentTiles() {
    for(int i = 0; i < 7; i++) {
      currentTiles.add(new Tile('?'));
    }
    for(Tile t : currentTiles) {
      float tileX = 497.5f + currentTiles.indexOf(t) * (t.getSize() + t.getSpacing()) - currentTiles.size() / 1.675f * t.getSize();
      float tileY = 1000 - t.getSize() * 1.5f;
      t.setPosition(tileX, tileY);
      t.setInitPosition(tileX, tileY);
    }
  }
  
  public void deselectTiles() {
    for(Tile t : currentTiles) {
      t.setSelected(false);
    }
  }
  
  public String currentTilesToString() {
    String res = "";
    for(Tile t : currentTiles) {
      res += t.getValue();
    }
    return res;
  }
  
  public ArrayList<Tile> getCurrentTiles() {
    return currentTiles;
  }

  public byte getId() {
    return id;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }
  
  public void addScore(int score) {
    this.score += score;
  }
  
  public void undoScore(int score) {
    this.score -= score;
  }
  
}
